<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routesh
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/branda', function () {
    return view('branda');
});

Route::get('/vendor', function () {
    return view('vendor');
});

Route::get('/gallery', function () {
    return view('gallery');
});

Auth::routes();

//Route::get('/branda', 'Auth::brandaController@branda');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/login', 'Auth::LoginController@index');
Auth::routes();
