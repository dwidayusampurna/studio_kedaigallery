<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'gallery';
    protected $primaryKey  = 'gallery_id';
    
    public $timestamps = true;

    protected $fillable = [
    	'gallery_id', 
        'gallery_nama',
        'gallery_foto',
    ];

    const CREATED_AT = "gallery_created_at";
    const UPDATED_AT = "gallery_updated_at";

    protected $dates = [
        "gallery_created_at",
        "gallery_updated_at"
    ];
}
